public class Application{
	public static void main(String[] args){
		Student[] section3 = new Student[3];
		Student student1 = new Student();
		Student student2 = new Student();
		
		student1.name = "Roberto Benmergui";
		student2.name = "Marco";
		student1.courseNum = 420;
		student2.courseNum = 420;
		student1.numClass = 5;
		student2.numClass = 5;
		
		System.out.println(student1.name + " " +student1.courseNum + " " +student1.numClass + " ");
		System.out.println(student2.name + " " +student2.courseNum + " " +student2.numClass + " ");
		
		/*
		Student.dropClass();
		Student.switchCourse(111);
		*/
		
		
		student1.dropClass();
		student1.switchCourse(111);
		student2.dropClass();
		student2.switchCourse(111);
		
		section3[0] = student1;
		section3[1] = student2;
		section3[2] = new Student();
		section3[2].name = "Janelle";
		section3[2].courseNum = 420;
		section3[2].numClass = 7;
		
		System.out.println(section3[2].name + " " + section3[2].courseNum + " " + section3[2].numClass);
	}
}